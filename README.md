# CubicWorldSimulator
![In-game screen](https://i.imgur.com/Sf8HSNZ.jpg =100px)

Use -XstartOnFirstThread JVM argument to launch game on Mac OS platform.

## This project is supported by
***
[![Atlassian logo](https://www.atlassian.com/it/dam/jcr:93075b1a-484c-4fe5-8a4f-942710e51760/Atlassian-horizontal-blue@2x-rgb.png =250x100)](https://www.atlassian.com/)

***

[![JProfiler logo](https://www.ej-technologies.com/images/product_banners/jprofiler_large.png)](https://www.ej-technologies.com/products/jprofiler/overview.html)  

[JProfiler](https://www.ej-technologies.com/products/jprofiler/overview.html) is an award-winning all-in-one [Java profiler](https://www.ej-technologies.com/products/jprofiler/overview.html). JProfiler's intuitive UI helps you find performance bottlenecks, pin down memory leaks and resolve threading issues.
***

[![YourKit logo](https://www.yourkit.com/images/yklogo.png)](https://www.yourkit.com/)                          

YourKit supports open source projects with innovative and intelligent tools
for monitoring and profiling Java and .NET applications.  
YourKit is the creator of [YourKit Java Profiler](https://www.yourkit.com/java/profiler/),
[YourKit .NET Profiler](https://www.yourkit.com/.net/profiler/),
and [YourKit YouMonitor](https://www.yourkit.com/youmonitor/).

***
